package org.xsts.tutorial.config;

import java.util.Map;
/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class TutorialConfig {
    public static final Integer PDF_WIDTH = 100;
    public static final Integer PDF_HEIGHT = 100;

    public static String homeDir() {
        Map<String,String> env = System.getenv();
        return env.get("HOME");
    }
}
