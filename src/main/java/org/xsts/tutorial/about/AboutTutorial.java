package org.xsts.tutorial.about;
/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
import org.xsts.core.about.InfoTransPub;

public class AboutTutorial {
    public static final String PROJECT_NAME = "org/xsts/tutorial";
    public static void main(String ... args) {

        StringBuilder aboutText;

        String aboutOrganization = new StringBuilder()
                .append("Organization: ")
                .append(InfoTransPub.FULL_ORGANIZATION_NAME)
                .toString();

        String aboutGroup = new StringBuilder()
                .append("Group: ")
                .append(InfoTransPub.SHORT_GROUP_NAME)
                .toString();

        String aboutProject = new StringBuilder()
                .append("Project: ")
                .append(PROJECT_NAME)
                .toString();

        System.out.println(aboutOrganization);
        System.out.println(aboutGroup);
        System.out.println(aboutProject);

    }
}
