package org.xsts.tutorial.gtfs.load;

import org.xsts.gtfs.gdl.loaders.BasicZipLoader;
import org.xsts.gtfs.gdl.loaders.processors.csv.agency.GTFSFirstAgencyProcessor;
import org.xsts.gtfs.gds.data.collections.GTFSAgencyList;
import org.xsts.gtfs.gds.data.names.GTFSFileNames;
import org.xsts.gtfs.gds.data.types.GTFSAgency;
import org.xsts.tutorial.config.TutorialConfig;

import java.nio.file.Path;
import java.nio.file.Paths;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class LoadAgencyFromZipFile {
    public static void main(String[] args) {
        // Make sure the path below is valid. Adjust the value to your needs
        // The gtfs data is flat, i.e. a folder containg the .txt files
        Path pathToZipContainingAgencyDotTxtFile = Paths.get(TutorialConfig.homeDir(),
                "tmp/gtfs/example.zip");

        GTFSAgencyList agencies = (GTFSAgencyList) BasicZipLoader.load(pathToZipContainingAgencyDotTxtFile,
                GTFSFileNames.AGENCY,
                new GTFSAgencyList(),
                new GTFSFirstAgencyProcessor(),
                false,
                null);

        GTFSAgency agency = agencies.getFirstAgency();
        System.out.println(agency);
    }
}
