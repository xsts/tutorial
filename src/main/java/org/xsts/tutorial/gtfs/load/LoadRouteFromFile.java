package org.xsts.tutorial.gtfs.load;

import org.xsts.gtfs.gdl.loaders.BasicDirLoader;
import org.xsts.gtfs.gdl.loaders.processors.csv.route.GTFSFirstRouteProcessor;
import org.xsts.gtfs.gds.data.collections.GTFSRouteList;
import org.xsts.gtfs.gds.data.names.GTFSFileNames;
import org.xsts.gtfs.gds.data.types.GTFSRoute;
import org.xsts.tutorial.config.TutorialConfig;

import java.nio.file.Path;
import java.nio.file.Paths;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class LoadRouteFromFile {
    public static void main(String[] args) {

        // Make sure the path below is valid. Adjust the value to your needs
        // The gtfs data is flat, i.e. a folder containg the .txt files
        Path pathToDirContainingRoutesDotTxtFile = Paths.get(TutorialConfig.homeDir(),
                "tmp/gtfs/load-example");

        GTFSRouteList routes = (GTFSRouteList) BasicDirLoader.load(pathToDirContainingRoutesDotTxtFile,
                GTFSFileNames.ROUTES,
                new GTFSRouteList(),
                new GTFSFirstRouteProcessor(),
                true,
                null);

        GTFSRoute route = routes.getFirstRoute();
        System.out.println(route);

    }
}
