package org.xsts.tutorial.pdf.schematics;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.xsts.pdf.tools.intval.PTIFillCircle;
import org.xsts.pdf.tools.intval.PTILine;
import org.xsts.pdf.tools.intval.PTIText;
import org.xsts.tutorial.config.TutorialConfig;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class BusLineExample01 {
    public static final Integer FACTOR = 3;
    public static final Integer A4_HEIGHT = 295 * FACTOR;
    public static final Integer A4_WIDTH = 210 * FACTOR;
    public static final Integer FONT_HEIGHT = 5 * FACTOR;
    public static final Integer X_POS = 30 * FACTOR;
    public static final Integer Y_POS = 30 * FACTOR;


    public static void main(String[] args) {
        BusLineExample01 processor = new BusLineExample01();
        processor.process();
    }

    public void process() {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "bus-stop-list-01.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(A4_WIDTH,A4_HEIGHT);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);
            verticalLine(contentStream);
            stopDots(contentStream);
            busStopList(contentStream);
            contentStream.close();

            doc.save(pdfFile);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void verticalLine(PDPageContentStream contentStream) throws IOException {
        PTIText text = new PTIText(contentStream);
        text
                .color(Color.black)
                .size(FONT_HEIGHT)
                .position(PTIText.LEFT)
                .angle(0)
                .font(PDType1Font.HELVETICA);
        PTILine line = new PTILine(contentStream);
        line
                .width(1)
                .color(Color.black);

        for ( int i = 1; i < BusStopNames.STOP_NAMES.length; i++){
            int xPos = X_POS - 1 * FONT_HEIGHT * 2;
            int yPos = A4_HEIGHT - Y_POS;
            line.from(xPos,yPos);
            yPos -=  i * FONT_HEIGHT * 2;
            line.to(xPos, yPos);
            line.draw();
        }
    }

    private void stopDots(PDPageContentStream contentStream) throws IOException {
        PTIFillCircle dot = new PTIFillCircle(contentStream);
        dot
                .fillColor(Color.black)
                .radius(FONT_HEIGHT/2);

        for ( int i = 0; i < BusStopNames.STOP_NAMES.length; i++){
            int xPos = X_POS - 1 * FONT_HEIGHT * 2;
            int yPos = A4_HEIGHT - Y_POS - i * FONT_HEIGHT * 2;
            dot.center(xPos, yPos);
            dot.draw();
        }
    }

    private void busStopList(PDPageContentStream contentStream) throws IOException {
        PTIText text = new PTIText(contentStream);
        text
                .color(Color.black)
                .size(FONT_HEIGHT)
                .position(PTIText.LEFT)
                .angle(0)
                .font(PDType1Font.HELVETICA);
        PTILine line = new PTILine(contentStream);
        line
                .width(1)
                .color(Color.black);

        for ( int i = 0; i < BusStopNames.STOP_NAMES.length; i++){
            int xPos = X_POS;
            int yPos = A4_HEIGHT - Y_POS - i * FONT_HEIGHT * 2;
            text
                    .at(xPos,yPos)
                    .text(BusStopNames.STOP_NAMES[i])
                    .draw();
        }
    }
}
