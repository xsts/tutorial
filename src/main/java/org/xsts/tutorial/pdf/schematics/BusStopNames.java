package org.xsts.tutorial.pdf.schematics;
/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class BusStopNames {
    public static final  String[] STOP_NAMES = {
            "Central Station",
            "Jan Hus Street",
            "Karolinen University",
            "Sports Arena",
            "Julius Caesar Bridge",
            "Old Town",
            "City Hall",
            "Hospital",
            "Optional Stop",
            "The Mall",
            "End of the Road"
    };
}
