package org.xsts.tutorial.pdf.turtle;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.pdf.tools.turtle.Turtle;
import org.xsts.tutorial.config.TutorialConfig;

import java.io.File;
import java.io.IOException;

/*
 * Group : XSTS
 * Project : Tutorial
 * This is a direct COPY/PASTE/TRANSLATION from a program written in Python on an internet  site
 * This example shows how to adapt python turtle code into XSTS-based Java code
 * See the original python program here: https://www.michael0x2a.com/blog/turtle-examples
 */
public class StarSpiralExample {
    public static final Integer PAGE_HEIGHT = 295;
    public static final Integer PAGE_WIDTH = 210;
    public static final Integer STAR_SIDE = 3;
    public static final Integer LEVEL_COUNT = 60;
    public static final Double FOUR_FIFTHS = Math.PI * 4 / 5;

    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "turtle-star-spiral.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(PAGE_WIDTH,PAGE_HEIGHT);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            Turtle turtle = new Turtle(contentStream);

            turtle.setPosition(PAGE_WIDTH/2-STAR_SIDE/2 ,PAGE_HEIGHT/2 + STAR_SIDE/2);
            turtle.setRotation(0.0);

            for ( int i = 0; i < LEVEL_COUNT; ++i ) {
                spiralArm(turtle, i);
            }

            contentStream.close();
            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    static void spiralArm(Turtle turtle, int level) throws IOException {
        turtle.forward(STAR_SIDE * level);
        turtle.right(FOUR_FIFTHS);
    }
}
