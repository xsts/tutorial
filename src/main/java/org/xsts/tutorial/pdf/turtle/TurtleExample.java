package org.xsts.tutorial.pdf.turtle;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.pdf.tools.turtle.Turtle;
import org.xsts.tutorial.config.TutorialConfig;

import java.io.File;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 * This TURTLE geometry program draws an equilateral triangle on the page.
 */
public class TurtleExample {

    public static final Integer PAGE_HEIGHT = 1000;
    public static final Integer PAGE_WIDTH = 1000;
    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "turtle.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(PAGE_HEIGHT,PAGE_WIDTH);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            Turtle turtle = new Turtle(contentStream);

            turtle.setPosition(PAGE_WIDTH/2,PAGE_HEIGHT/2);
            turtle.setRotation(0.0);
            turtle.forward(100);
            turtle.left(Math.PI * 2/3);
            turtle.forward(100);
            turtle.left(Math.PI * 2/3);
            turtle.forward(100);

            contentStream.close();
            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }


    }
}
