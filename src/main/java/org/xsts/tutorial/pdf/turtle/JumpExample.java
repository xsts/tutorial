package org.xsts.tutorial.pdf.turtle;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.pdf.tools.turtle.Turtle;
import org.xsts.tutorial.config.TutorialConfig;

import java.io.File;
import java.io.IOException;

/*
 * Group : XSTS
 * Project : Tutorial
 * This is a direct COPY/PASTE/TRANSLATION from a program written in Python on an internet  site
 * This example shows how to adapt python turtle code into XSTS-based Java code
 * See the original python program here: https://www.michael0x2a.com/blog/turtle-examples
 */
public class JumpExample {
    public static final Integer PAGE_HEIGHT = 295;
    public static final Integer PAGE_WIDTH = 210;
    public static final Integer FULL_CIRCLE = 360;
    public static final Double THIRTY_DEGREES = Math.PI *30 / 180;
    public static final Double SIXTY_DEGREES = Math.PI *60 / 180;
    public static final Double TWO_DEGREES = Math.PI *2 / 180;

    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "turtle-jump.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(PAGE_WIDTH,PAGE_HEIGHT);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            Turtle turtle = new Turtle(contentStream);
            turtle.setPosition(PAGE_WIDTH/2 ,PAGE_HEIGHT/2);
            turtle.setRotation(0.0);

            for ( int i = 0; i < FULL_CIRCLE; i+= 2 ) {
                zigzag(turtle);
            }

            contentStream.close();
            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    static void zigzag(Turtle turtle ) throws IOException {
        // part of the central discus
        turtle.forward(60);

        // part of the first ring
        turtle.right(THIRTY_DEGREES);
        turtle.forward(20);

        // part of the second ring
        turtle.left(SIXTY_DEGREES);
        turtle.forward(30);

        // turn the turtle around the center of the discus
        turtle.right(THIRTY_DEGREES);
        turtle.setPosition(PAGE_WIDTH/2 ,PAGE_HEIGHT/2);
        turtle.right(TWO_DEGREES);
    }
}
