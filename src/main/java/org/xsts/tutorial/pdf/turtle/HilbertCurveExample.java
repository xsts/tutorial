package org.xsts.tutorial.pdf.turtle;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.pdf.tools.turtle.Turtle;
import org.xsts.tutorial.config.TutorialConfig;

import java.io.File;
import java.io.IOException;

/*
 * Group : XSTS
 * Project : Tutorial
 * This is a direct COPY/PASTE/TRANSLATION from a program written in Python on the  STACK OVERFLOW site
 * This example shows how to adapt python turtle code into XSTS-based Java code
 * See the original Python program here: https://stackoverflow.com/questions/46565729/turtle-graphics-with-recursion
 */
public class HilbertCurveExample {

    public static final Integer DEPTH = 7;
    public static final Integer SIZE = 1 << DEPTH;
    public static final Integer PAGE_HEIGHT = SIZE;
    public static final Integer PAGE_WIDTH = SIZE;

    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "hilbert.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(PAGE_HEIGHT,PAGE_WIDTH);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            Turtle turtle = new Turtle(contentStream);

            turtle.setPosition(PAGE_WIDTH/2,PAGE_HEIGHT/2);
            turtle.setPosition(0.0,0.0);
            turtle.setRotation(0.0);
            int depth = DEPTH;
            int size = SIZE;
            hilbert(depth, turtle, Math.PI/2 );


            contentStream.close();
            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }


    }

    private static void hilbert(int n,Turtle turtle, double angle ) throws IOException {
        if (n <= 0)
            return;

        turtle.left(angle);
        hilbert(n - 1, turtle, -angle);
        turtle.forward(1);
        turtle.right(angle);
        hilbert(n - 1, turtle, angle);
        turtle.forward(1);
        hilbert(n - 1, turtle, angle);
        turtle.right(angle);
        turtle.forward(1);
        hilbert(n - 1, turtle, -angle);
        turtle.left(angle);
    }
}
