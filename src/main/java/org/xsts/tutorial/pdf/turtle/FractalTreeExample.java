package org.xsts.tutorial.pdf.turtle;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.pdf.tools.turtle.Turtle;
import org.xsts.tutorial.config.TutorialConfig;

import java.io.File;
import java.io.IOException;

/*
 * Group : XSTS
 * Project : Tutorial
 * This is a direct COPY/PASTE/TRANSLATION from a program written in Python on the  SIMPLIFIED PYTHON site
 * This example shows how to adapt python turtle code into XSTS-based Java code
 * See the original python program here: https://www.simplifiedpython.net/python-turtle-module/
 */
public class FractalTreeExample {
    public static final Integer PAGE_HEIGHT = 1000;
    public static final Integer PAGE_WIDTH = 1000;

    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "tree.pdf");

        try (PDDocument doc = new PDDocument()){

            page(doc,200,1,2);
            page(doc,200,2,3);
            page(doc,200,3,4);


            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }


    }

    private static void page(PDDocument doc, int branchLength, int factorMul, int factorDiv) throws IOException {
        PDRectangle rect = new PDRectangle(PAGE_HEIGHT,PAGE_WIDTH);
        PDPage page = new PDPage(rect);
        doc.addPage(page);
        PDPageContentStream contentStream = new PDPageContentStream(doc, page);

        Turtle turtle = new Turtle(contentStream);
        turtle.setPosition(PAGE_WIDTH/2,PAGE_HEIGHT/4);
        turtle.setRotation(0.0);
        turtle.left(Math.PI/2);
        tree(turtle, branchLength,factorMul,factorDiv);

        contentStream.close();
    }

    private static void  tree(Turtle turtle, int branchLength, int factorMul, int factorDiv) throws IOException {
        if(branchLength < 1)
            return;
        turtle.forward(branchLength);
        turtle.left(Math.PI/6);
        tree(turtle, factorMul*branchLength/factorDiv,factorMul,factorDiv );
        turtle.right(Math.PI/3);
        tree(turtle, factorMul*branchLength/factorDiv,factorMul,factorDiv);
        turtle.left(Math.PI/6);
        turtle.backward(branchLength);
    }


}
