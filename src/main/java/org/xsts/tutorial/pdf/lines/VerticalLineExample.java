package org.xsts.tutorial.pdf.lines;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.pdf.tools.intval.PTILine;
import org.xsts.tutorial.config.TutorialConfig;

import java.awt.Color;
import java.io.File;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class VerticalLineExample {
    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "vertical-line.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(TutorialConfig.PDF_WIDTH,TutorialConfig.PDF_HEIGHT);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            new PTILine(contentStream)
                    .color(Color.lightGray)
                    .width(1)
                    .from(50,25)
                    .to(50,75)
                    .draw();

            contentStream.close();
            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
