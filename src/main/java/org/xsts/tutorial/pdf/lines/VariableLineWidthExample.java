package org.xsts.tutorial.pdf.lines;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.pdf.tools.intval.PTILine;
import org.xsts.tutorial.config.TutorialConfig;

import java.awt.Color;
import java.io.File;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class VariableLineWidthExample {
    public static final Integer A4_HEIGHT = 295;
    public static final Integer A4_WIDTH = 210;
    public static final Integer LINE_COUNT = 20;
    public static final Integer LINE_SPACE = 5;
    public static final Integer PAGE_BOTTOM = 0;



    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "variable-line-width.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(A4_WIDTH, A4_HEIGHT);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            PTILine line = new PTILine(contentStream)
                    .color(Color.lightGray);
            int yPos = PAGE_BOTTOM;
            for ( int i = 0; i <= LINE_COUNT; ++i) {
                line
                        .width(i)
                        .from(0, yPos)
                        .to(A4_WIDTH, yPos)
                        .draw();

                yPos += LINE_SPACE;
                yPos += i;

            }

            contentStream.close();
            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }


    }
}

