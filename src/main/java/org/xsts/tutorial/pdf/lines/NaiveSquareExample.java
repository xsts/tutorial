package org.xsts.tutorial.pdf.lines;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.pdf.tools.intval.PTILine;
import org.xsts.tutorial.config.TutorialConfig;

import java.awt.Color;
import java.io.File;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class NaiveSquareExample {
    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "naive-square.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(TutorialConfig.PDF_WIDTH,TutorialConfig.PDF_HEIGHT);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            new PTILine(contentStream)
                    .color(Color.blue)
                    .width(1)
                    .from(25,25).to(25,75).draw()
                    .from(25,75).to(75,75).draw()
                    .from(75,75).to(75,25).draw()
                    .from(75,25).to(25,25).draw();

            contentStream.close();

            page = new PDPage(rect);
            doc.addPage(page);
            contentStream = new PDPageContentStream(doc, page);

            new PTILine(contentStream)
                    .color(Color.blue)
                    .width(2)
                    .from(25,25).to(25,75).draw()
                    .from(25,75).to(75,75).draw()
                    .from(75,75).to(75,25).draw()
                    .from(75,25).to(25,25).draw();

            contentStream.close();

            page = new PDPage(rect);
            doc.addPage(page);
            contentStream = new PDPageContentStream(doc, page);

            new PTILine(contentStream)
                    .color(Color.blue)
                    .width(2)
                    .from(25+1,25).to(25+1,75).draw()
                    .from(25,75-1).to(75,75-1).draw()
                    .from(75-1,75).to(75-1,25).draw()
                    .from(75,25+1).to(25,25+1).draw();

            contentStream.close();

            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }

    }
}
