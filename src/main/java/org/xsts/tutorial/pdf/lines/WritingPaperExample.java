package org.xsts.tutorial.pdf.lines;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.xsts.pdf.tools.intval.PTILine;
import org.xsts.tutorial.config.TutorialConfig;

import java.awt.Color;
import java.io.File;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class WritingPaperExample {
    public static final Integer A4_HEIGHT = 295;
    public static final Integer A4_WIDTH = 210;

    public static final Integer LEFT_MARGIN = 30;
    public static final Integer TOP_MARGIN = 35;
    public static final Integer BOTTOM_MARGIN = 20;

    public static final Integer PAGE_BOTTOM = 0;
    public static final Integer PAGE_TOP = A4_HEIGHT;
    public static final Integer PAGE_LEFT = 0;
    public static final Integer PAGE_RIGHT = A4_WIDTH;

    public static final Integer ROW_HEIGHT = 13;
    public static final Integer LINE_THICKNESS = 1;

    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "writing-paper.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(A4_WIDTH, A4_HEIGHT);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            drawVerticalLine(contentStream);
            drawHorizontalLines(contentStream);

            contentStream.close();
            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void drawHorizontalLines(PDPageContentStream contentStream) throws Exception {
        int rowYPos = BOTTOM_MARGIN;
        int rowMaxPos = PAGE_TOP - TOP_MARGIN;
        PTILine rowLine = new PTILine(contentStream)
                .color(Color.blue)
                .width(LINE_THICKNESS);

        while(rowYPos < rowMaxPos) {
            rowLine
                    .from(PAGE_LEFT,rowYPos)
                    .to(PAGE_RIGHT,rowYPos)
                    .draw();
            rowYPos += ROW_HEIGHT;
        }
    }

    private static void drawVerticalLine(PDPageContentStream contentStream) throws Exception {
        new PTILine(contentStream)
                .color(Color.blue)
                .width(LINE_THICKNESS)
                .from(LEFT_MARGIN,PAGE_BOTTOM)
                .to(LEFT_MARGIN,PAGE_TOP)
                .draw();
    }
}
