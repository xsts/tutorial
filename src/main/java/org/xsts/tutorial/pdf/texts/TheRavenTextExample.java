package org.xsts.tutorial.pdf.texts;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.xsts.pdf.tools.intval.PTIText;
import org.xsts.tutorial.config.TutorialConfig;

import java.awt.Color;
import java.io.File;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 *
 * In this example, I put up to 4 stanzas on each page
 * Each stanza is 6 verses long, however, as you can see, the plotting is  dynamic and
 * the  magic number 6 doesn't appear in the code. See for yourself and add or remove verses
 * in a stanza.
 * Note that you can select the text in the resulting PDF file.
 *
 * Just to see the ugly redundancy of the classical object dot method call syntax,
 * note how I used the textTool element.
 */
public class TheRavenTextExample {
    public static final Integer FACTOR = 3;
    public static final Integer A4_HEIGHT = 295 * FACTOR;
    public static final Integer A4_WIDTH = 210 * FACTOR;
    public static final Integer TOP_MARGIN = 50 * FACTOR;
    public static final Integer LEFT_MARGIN = 30 * FACTOR;

    public static final Integer VERSE_GROUP_HEIGHT = 100;
    public static final Integer VERSE_GROUP_PADDING = 30;
    public static final Integer VERSE_FONT_HEIGHT = 5 * FACTOR;


    public static final Integer GROUP_COUNT = 4;
    public static final  String[] [] THE_RAVEN = {
            // Here I jump over some verses, just to make the poem shorter.
            {
                "Once upon a midnight dreary, while I pondered, weak and weary,",
                "Over many a quaint and curious volume of forgotten lore—",
                "While I nodded, nearly napping, suddenly there came a tapping,",
                "As of some one gently rapping, rapping at my chamber door.",
                "'Tis some visitor,” I muttered, tapping at my chamber door—",
                "Only this and nothing more."
            },
            {
                "Ah, distinctly I remember it was in the bleak December;",
                "And each separate dying ember wrought its ghost upon the floor.",
                "Eagerly I wished the morrow;—vainly I had sought to borrow",
                "From my books surcease of sorrow—sorrow for the lost Lenore—",
                "For the rare and radiant maiden whom the angels name Lenore—",
                "Nameless here for evermore."
            },
            {
                "And the silken, sad, uncertain rustling of each purple curtain",
                "Thrilled me—filled me with fantastic terrors never felt before;",
                "So that now, to still the beating of my heart, I stood repeating",
                "’Tis some visitor entreating entrance at my chamber door—",
                "Some late visitor entreating entrance at my chamber door;—",
                "This it is and nothing more."
            },
            {
                "Presently my soul grew stronger; hesitating then no longer,",
                "Sir, said I, or Madam, truly your forgiveness I implore;",
                "But the fact is I was napping, and so gently you came rapping,",
                "And so faintly you came tapping, tapping at my chamber door,",
                "That I scarce was sure I heard you—here I opened wide the door;—",
                "Darkness there and nothing more."
            },
            {
                "Deep into that darkness peering, long I stood there wondering, fearing,",
                "Doubting, dreaming dreams no mortal ever dared to dream before;",
                "But the silence was unbroken, and the stillness gave no token,",
                "And the only word there spoken was the whispered word, Lenore?",
                "This I whispered, and an echo murmured back the word, Lenore!”—",
                "Merely this and nothing more."
            },
            {
                "Back into the chamber turning, all my soul within me burning,",
                "Soon again I heard a tapping somewhat louder than before.",
                "Surely, said I, surely that is something at my window lattice;",
                "Let me see, then, what thereat is, and this mystery explore—",
                "Let my heart be still a moment and this mystery explore;—",
                "’Tis the wind and nothing more!"
            },
            {
                "Startled at the stillness broken by reply so aptly spoken,",
                "Doubtless, said I, what it utters is its only stock and store",
                "Caught from some unhappy master whom unmerciful Disaster",
                "Followed fast and followed faster till his songs one burden bore—",
                "Till the dirges of his Hope that melancholy burden bore",
                "Of ‘Never—nevermore’."
            },
            {
                "But the Raven still beguiling all my fancy into smiling,",
                "Straight I wheeled a cushioned seat in front of bird, and bust and door;",
                "Then, upon the velvet sinking, I betook myself to linking",
                "Fancy unto fancy, thinking what this ominous bird of yore—",
                "What this grim, ungainly, ghastly, gaunt, and ominous bird of yore",
                "Meant in croaking Nevermore."
            }
    };

    public static void main(String ... args) {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "text-printing.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(A4_WIDTH, A4_HEIGHT);
            PDPage page;
            PDPageContentStream contentStream;

            int pageGroupCount = 0;

            int verseGroupCount = 1;
            int groupPos;
            int pageCount = THE_RAVEN.length / GROUP_COUNT;
            if ( (THE_RAVEN.length % GROUP_COUNT) > 0)
                pageCount ++;


            for ( int pageNumber = 0; pageNumber < pageCount; ++pageNumber) {
                page = new PDPage(rect);
                doc.addPage(page);
                contentStream = new PDPageContentStream(doc, page);
                PTIText textTool = new PTIText(contentStream);
                textTool.font(PDType1Font.HELVETICA);
                textTool.color(Color.blue);
                textTool.angle(0);
                textTool.position(PTIText.LEFT);
                textTool.size(VERSE_FONT_HEIGHT);

                for (pageGroupCount = 0; pageGroupCount < GROUP_COUNT; ++pageGroupCount) {
                    groupPos = pageNumber * GROUP_COUNT + pageGroupCount;
                    if ( groupPos >= THE_RAVEN.length )
                        break;

                    int groupYPos = A4_HEIGHT - TOP_MARGIN - pageGroupCount* (VERSE_GROUP_HEIGHT + VERSE_GROUP_PADDING);
                    int groupXPos =LEFT_MARGIN;

                    String[] verses = THE_RAVEN[groupPos];
                    for ( int verseNumber = 0; verseNumber < verses.length; ++verseNumber) {
                        int verseYPos = groupYPos - verseNumber * VERSE_FONT_HEIGHT;
                        textTool.at(groupXPos,verseYPos);
                        textTool.text(verses[verseNumber]);
                        textTool.draw();
                    }

                }

                contentStream.close();
            }






            doc.save(pdfFile);

        } catch(Exception e) {
            e.printStackTrace();
        }


    }
}

/*





 */