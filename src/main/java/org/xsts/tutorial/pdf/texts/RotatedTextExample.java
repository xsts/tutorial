package org.xsts.tutorial.pdf.texts;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.xsts.pdf.tools.intval.PTILine;
import org.xsts.pdf.tools.intval.PTIText;
import org.xsts.tutorial.config.TutorialConfig;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class RotatedTextExample {
    public static void main(String[] args) {
        RotatedTextExample processor = new RotatedTextExample();
        processor.process();
    }
    public void process() {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "testing-pdf-rotated-text.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(100,100);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);
            rotationTestOne(contentStream);
            contentStream.close();

            page = new PDPage(rect);
            doc.addPage(page);
            contentStream = new PDPageContentStream(doc, page);
            rotationTestTwo(contentStream);
            contentStream.close();

            page = new PDPage(rect);
            doc.addPage(page);
            contentStream = new PDPageContentStream(doc, page);
            rotationTestThree(contentStream);
            contentStream.close();

            page = new PDPage(rect);
            doc.addPage(page);
            contentStream = new PDPageContentStream(doc, page);
            rotationTestFour(contentStream);
            contentStream.close();

            page = new PDPage(rect);
            doc.addPage(page);
            contentStream = new PDPageContentStream(doc, page);
            rotationTestFive(contentStream);
            contentStream.close();

            page = new PDPage(rect);
            doc.addPage(page);
            contentStream = new PDPageContentStream(doc, page);
            rotationTestSix(contentStream);
            contentStream.close();

            doc.save(pdfFile);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    private void rotationTestOne(PDPageContentStream contentStream) throws IOException {
        logo(contentStream);

        new PTILine(contentStream)
                .color(Color.black).width(1)
                .from(40,50).to(60,50)
                .draw()
                .from(50,40).to(50,60)
                .draw();

        for ( int i = 0; i < 360; ){
            StringBuilder sb = new StringBuilder();
            sb.append("------------ text ").append(i).append("°");
            new PTIText(contentStream).at(50,50)
                    .font(PDType1Font.HELVETICA_BOLD)
                    .text(sb.toString())
                    .color(new Color((i*10)%256,0,0))
                    .angle(i)
                    .position(PTIText.LEFT)
                    .size(5)
                    .draw();
            i+=10;
        }

        new PTIText(contentStream).at(1,2)
                .font(PDType1Font.HELVETICA_BOLD)
                .text("Rotation of left align text")
                .color(Color.black)
                .angle(0)
                .position(PTIText.LEFT)
                .size(5)
                .draw();

    }



    private void rotationTestTwo(PDPageContentStream contentStream) throws IOException {
        logo(contentStream);

        new PTILine(contentStream)
                .color(Color.black).width(1)
                .from(40,50).to(60,50)
                .draw()
                .from(50,40).to(50,60)
                .draw();

        for ( int i = 0; i < 180; ){
            StringBuilder sb = new StringBuilder();
            sb
                    .append("centered ------            ------ deretnec");
            new PTIText(contentStream).at(50,50)
                    .font(PDType1Font.HELVETICA_BOLD)
                    .text(sb.toString())
                    .color(new Color((i*20)%256,0,0))
                    .angle(i)
                    .position(PTIText.CENTER)
                    .size(5)
                    .draw();
            i+=20;
        }

        new PTIText(contentStream).at(50,2)
                .font(PDType1Font.HELVETICA_BOLD)
                .text("Rotation of center aligned text")
                .color(Color.black)
                .angle(0)
                .position(PTIText.CENTER)
                .size(5)
                .draw();

    }

    private void rotationTestThree(PDPageContentStream contentStream) throws IOException {
        logo(contentStream);

        new PTILine(contentStream)
                .color(Color.black).width(1)
                .from(40,50).to(60,50)
                .draw()
                .from(50,40).to(50,60)
                .draw();

        for ( int i = 0; i < 180; ){
            StringBuilder sb = new StringBuilder();
            sb
                    .append("this text is centered");
            new PTIText(contentStream).at(50,50)
                    .font(PDType1Font.COURIER)
                    .text(sb.toString())
                    .color(new Color((i*20)%256,0,0))
                    .angle(i)
                    .position(PTIText.CENTER)
                    .size(5)
                    .draw();
            i+=20;
        }

        new PTIText(contentStream).at(50,2)
                .font(PDType1Font.HELVETICA_BOLD)
                .text("Rotation of center aligned text")
                .color(Color.black)
                .angle(0)
                .position(PTIText.CENTER)
                .size(5)
                .draw();

    }

    private void rotationTestFour(PDPageContentStream contentStream) throws IOException {
        logo(contentStream);

        new PTILine(contentStream)
                .color(Color.black).width(1)
                .from(40,50).to(60,50)
                .draw()
                .from(50,40).to(50,60)
                .draw();

        for ( int i = 0; i < 360; ){
            StringBuilder sb = new StringBuilder();
            sb
                    .append("this text is right aligned");
            new PTIText(contentStream).at(50,50)
                    .font(PDType1Font.TIMES_ROMAN)
                    .text(sb.toString())
                    .color(new Color((i*20)%256,0,0))
                    .angle(i)
                    .position(PTIText.RIGHT)
                    .size(5)
                    .draw();
            i+=20;
        }

        new PTIText(contentStream).at(100,2)
                .font(PDType1Font.TIMES_ROMAN)
                .text("Rotation of right aligned text")
                .color(Color.black)
                .angle(0)
                .position(PTIText.RIGHT)
                .size(5)
                .draw();

    }


    private void rotationTestFive(PDPageContentStream contentStream) throws IOException {
        logo(contentStream);

        new PTILine(contentStream)
                .color(Color.black).width(1)
                .from(40,50).to(60,50)
                .draw()
                .from(50,40).to(50,60)
                .draw();

        for ( int i = 0; i < 12; i++ ){
            StringBuilder sb = new StringBuilder();
            sb
                    .append("                   ").append((12-i) );
            new PTIText(contentStream).at(50,50)
                    .font(PDType1Font.TIMES_ROMAN)
                    .text(sb.toString())
                    .color(Color.black)
                    .angle(i*30+90)
                    .position(PTIText.LEFT)
                    .size(5)
                    .draw();
        }

        new PTIText(contentStream).at(1,2)
                .font(PDType1Font.TIMES_ROMAN)
                .text("The clock")
                .color(Color.black)
                .angle(0)
                .position(PTIText.LEFT)
                .size(5)
                .draw();

    }

    private void rotationTestSix(PDPageContentStream contentStream) throws IOException {
        logo(contentStream);
        new PTILine(contentStream)
                .color(Color.black).width(1)
                .from(40,50).to(60,50)
                .draw()
                .from(50,40).to(50,60)
                .draw();

        for ( int i = 0; i < 360; ){
            StringBuilder sb = new StringBuilder();
            sb
                    .append("           --------------------------------- ").append(i).append("°");
            new PTIText(contentStream).at(50,50)
                    .font(PDType1Font.TIMES_ROMAN)
                    .text(sb.toString())
                    .color(Color.black)
                    .angle(i)
                    .position(PTIText.LEFT)
                    .size(3)
                    .draw();
            i+=3;
        }

        new PTIText(contentStream).at(1,2)
                .font(PDType1Font.TIMES_ROMAN)
                .text("density")
                .color(Color.black)
                .angle(0)
                .position(PTIText.LEFT)
                .size(5)
                .draw();

    }


    private void logo(PDPageContentStream contentStream) throws IOException {
        String xstsText = "THIS IS XSTS";
        Color wastedColor = new Color(220,220,220);
        for ( int xx = 0; xx < 10; xx++){
            for ( int yy = 0; yy < 10; yy++){
                int xpos = xx*30;
                int ypos = yy * 10;
                new PTIText(contentStream).at(xpos,ypos)
                        .font(PDType1Font.TIMES_ROMAN)
                        .text(xstsText)
                        .color(wastedColor)
                        .angle(0)
                        .position(PTIText.LEFT)
                        .size(3)
                        .draw();
            }
        }
    }
}
