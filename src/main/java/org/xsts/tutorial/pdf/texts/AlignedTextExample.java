package org.xsts.tutorial.pdf.texts;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.xsts.pdf.tools.intval.PTILine;
import org.xsts.pdf.tools.intval.PTIText;
import org.xsts.tutorial.config.TutorialConfig;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

/*
 * Group : XSTS
 * Project : Tutorial
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
public class AlignedTextExample {
    public static final Integer FACTOR = 3;
    public static final Integer A4_HEIGHT = 295 * FACTOR;
    public static final Integer A4_WIDTH = 210 * FACTOR;
    public static final Integer FONT_HEIGHT = 5 * FACTOR;


    public static void main(String[] args) {
        AlignedTextExample processor = new AlignedTextExample();
        processor.process();
    }

    public void process() {

        File pdfFile = new File(TutorialConfig.homeDir(),
                "aligned-text.pdf");

        try (PDDocument doc = new PDDocument()){
            PDRectangle rect = new PDRectangle(A4_WIDTH,A4_HEIGHT);
            PDPage page = new PDPage(rect);
            doc.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(doc, page);

            drawCrossHairs(contentStream);
            drawLeftAligned(contentStream);
            drawCentered(contentStream);
            drawRightAligned(contentStream);



            PTIText text = new PTIText(contentStream);
            text
                    .angle(0)
                    .color(Color.blue)
                    .font(PDType1Font.HELVETICA)
                    .size(FONT_HEIGHT);

            text.at(0,A4_HEIGHT/2).text("This text is left aligned").draw();



            contentStream.close();

            doc.save(pdfFile);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void drawLeftAligned(PDPageContentStream contentStream) throws IOException {
        PTIText text = new PTIText(contentStream);
        text
                .angle(0)
                .color(Color.blue)
                .font(PDType1Font.HELVETICA)
                .size(FONT_HEIGHT)
                .at(0,A4_HEIGHT/2)
                .position(PTIText.LEFT)
                .text("This text is left aligned")
                .draw();
    }

    private void drawRightAligned(PDPageContentStream contentStream) throws IOException {
        PTIText text = new PTIText(contentStream);
        text
                .angle(0)
                .color(Color.blue)
                .font(PDType1Font.HELVETICA)
                .size(FONT_HEIGHT)
                .at(A4_WIDTH,A4_HEIGHT/2)
                .position(PTIText.RIGHT)
                .text("This text is right aligned")
                .draw();
    }

    private void drawCentered(PDPageContentStream contentStream) throws IOException {
        PTIText text = new PTIText(contentStream);
        text
                .angle(0)
                .color(Color.blue)
                .font(PDType1Font.HELVETICA)
                .size(FONT_HEIGHT)
                .at(A4_WIDTH/2,A4_HEIGHT/2)
                .position(PTIText.CENTER)
                .text("This text is centered")
                .draw();
    }

    private void drawCrossHairs(PDPageContentStream contentStream) throws IOException {
        PTILine line = new PTILine(contentStream);
        line
                .color(Color.black)
                .width(1);

        line
                .from(0, A4_HEIGHT/2)
                .to(A4_WIDTH,A4_HEIGHT/2)
                .draw();

        line
                .from(A4_WIDTH/2, 0)
                .to(A4_WIDTH/2,A4_HEIGHT)
                .draw();

    }

}
